package configs

import (
	"fmt"
	"io/ioutil"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	PrivateKey []byte
)

func Init() {

	PrivateKey, _ = ioutil.ReadFile("conf/tes.rsa")
}

func ParseJwt(myToken string, myKey string) ([]byte, bool) {
	token, err := jwt.Parse(myToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(myKey), nil
	})

	if err == nil && token.Valid {
		fmt.Println("Your token is valid.  I like your style.")
		return []byte(myKey), true
	}
	fmt.Println("This token is terrible!  I cannot accept this.")
	return nil, false

}
