package configs

import (
	"fmt"
	"log"
	"time"

	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

// Db global koneksi
var (
	// this one gonna contain an open connection
	// make sure to call Connect() before using it
	Condb *gorm.DB

	// Env *config.Config
)

// TypeDatabase ...
var TypeDatabase string

// var LogError lumberjack

func init() {
	log.SetOutput(&lumberjack.Logger{
		Filename:   "log/error.log",
		MaxSize:    10, // megabytes
		MaxBackups: 100,
		MaxAge:     1, //days
	})
}

func getTimeNow() time.Time {
	now := time.Now()
	return (now)
}

// CobaKonek ke database
func CobaKonek() (*gorm.DB, error) {
	// Env, errRead := config.ReadDefault("pengaturan.env")
	var db *gorm.DB
	var err error
	beego.AppConfig.String("TYPE_DATABASE")
	typeDatabase := beego.AppConfig.String("TYPE_DATABASE")
	if typeDatabase == "mysql" {
		user := beego.AppConfig.String("DB_USERNAME")
		pass := beego.AppConfig.String("DB_PASSWORD")
		database := beego.AppConfig.String("DB_DATABASE")
		host := beego.AppConfig.String("DB_HOST")
		port := beego.AppConfig.String("DB_PORT")
		db, err = gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
		if err != nil {
			db.Exec("UPDATE `GLOBAL_VARIABLES` SET `VARIABLE_VALUE`=`InnoDB` WHERE `VARIABLE_NAME`=`DEFAULT_STORAGE_ENGINE`")

		}
	} else if typeDatabase == "sqlite3" {
		database := beego.AppConfig.String("DB_DATABASE")
		db, err = gorm.Open("sqlite3", database+".db")
	} else if typeDatabase == "postgre" {
		user := beego.AppConfig.String("DB_USERNAME")
		pass := beego.AppConfig.String("DB_PASSWORD")
		database := beego.AppConfig.String("DB_DATABASE")
		host := beego.AppConfig.String("DB_HOST")
		tempStr := "host=" + host + "user=" + user + "dbname=" + database + "sslmode=disable password=" + pass
		db, err = gorm.Open("postgres", tempStr)

	}
	if err != nil {
		// log.Fatal(err)
		panic(err)

	} else {
		log.Println("terkoneksi dengan database")

	}
	db.LogMode(false)

	// db.DB().SetConnMaxLifetime(0)
	// db.DB().SetMaxIdleConns(100)
	// db.DB().SetMaxOpenConns(5)
	// db.DB().SetMaxIdleConns(10)
	// db.DB().SetMaxOpenConns(100)

	// db.DB().SetMaxIdleConns(0)
	// db.DB().SetMaxOpenConns(0)
	db.Exec("PRAGMA foreign_keys = ON")
	// db.Exec("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))")
	db.LogMode(true)
	if db.HasTable("users") == false {
		fmt.Println("Created table tag_lists")
		db.AutoMigrate(&User{})
		passPord, _ := bcrypt.GenerateFromPassword([]byte("sundul"), 14)
		user := User{
			Username: "edy",
			Nama:     "edyprasetiyo",
			Email:    "edot92@gmail.com",
			Password: string(passPord),
		}
		db.Create(&user)
	}
	// if db.HasTable("jwt_tokens") == false {
	// 	fmt.Println("Created table controller_lists")
	// 	db.AutoMigrate(&JwtToken{})
	// }

	// if db.HasTable("role") == false {
	// 	fmt.Println("Created table controller_lists")
	// 	db.AutoMigrate(&Role{})
	// }
	// Add foreign key
	// 1st param : foreignkey field
	// 2nd param : destination table(id)
	// 3rd param : ONDELETE
	// 4th param : ONUPDATE
	// AddForeignKey Add foreign key to the given scope, e.g:
	//     db.Model(&User{}).AddForeignKey("city_id", "cities(id)", "RESTRICT", "RESTRICT")
	// func (s *DB) AddForeignKey(field string, dest string, onDelete string, onUpdate string) *DB {
	// 	scope := s.clone().NewScope(s.Value)
	// 	scope.addForeignKey(field, dest, onDelete, onUpdate)
	// 	return scope.db
	// }
	// https://github.com/jinzhu/gorm/blob/master/main.go
	// db.Model(&JwtToken{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
	// db.Model(&Role{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")

	Condb = db
	return db, nil
}
