package configs

import (
	"time"
)

type Model struct {
	ID        uint `gorm:"primary_key;AUTO_INCREMENT"`
	CreatedAt time.Time
	UpdatedAt time.Time
	//   DeletedAt *time.Time
}

type User struct {
	ID       uint   `gorm:"primary_key;AUTO_INCREMENT"`
	Username string `gorm:"not null;type:varchar(50)"`
	Nama     string `gorm:"not null;type:varchar(50)"`
	Email    string `gorm:"not null;unique"`
	Password string `gorm:"not null"`
	// RolesUserIDUsersIDForeign     uint   `gorm:"AUTO_INCREMENT"`
	// JwtTokensUserIDUsersIDForeign uint   `gorm:"AUTO_INCREMENT"`

	JwtToken JwtToken `gorm:"ForeignKey:UserID"` // one to many
	Role     Role     `gorm:"ForeignKey:UserID"` // one to many
}
type JwtToken struct {
	ID uint `gorm:"primary_key;AUTO_INCREMENT"`

	UserID    uint
	Exp       time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}
type Role struct {
	ID uint `gorm:"primary_key;AUTO_INCREMENT"`

	UserID    uint
	Role      string `gorm:"not null" sql:"DEFAULT:'member'"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
