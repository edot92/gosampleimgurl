package configs

type listEndPointAPI struct {
	TopicListAPI       string
	TopicDetailAPI     string
	TopicGaleryItemAPI string
}

type imgurl struct {
	ClientID     string
	ClientSecret string
	Endpoint     listEndPointAPI
}

// Imgur ClientID get from registration
var Imgur imgurl

func init() {
	domainTarget := "https://api.imgur.com"
	Imgur.ClientID = "Client-Id 7b1fa2e301e5907"
	Imgur.ClientSecret = ""
	Imgur.Endpoint.TopicListAPI = domainTarget + "/3/topics/defaults"
	Imgur.Endpoint.TopicDetailAPI = domainTarget + "/3/topics/"
	Imgur.Endpoint.TopicGaleryItemAPI = domainTarget + "/3/topics/" //{topicid}/{itemid}
}
