package models

import (
	configs "gitlab.com/edot92/gosampleimgurl/configs"
)

// Getuser return true if match or any /
func GetUser(username string) (bool, string) {
	var user configs.User
	configs.Condb.First(&user, "username=?", username)
	if user.Nama != "" {
		return true, user.Password
	}
	return false, ""
}
