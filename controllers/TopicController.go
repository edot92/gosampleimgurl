package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/astaxie/beego"
	"gitlab.com/edot92/gosampleimgurl/configs"
	"gitlab.com/edot92/gosampleimgurl/views/go"
)

type TopicController struct {
	beego.Controller
}

func (c *TopicController) GetTopicList() {

	// handle error
	client := &http.Client{}
	urlGet := configs.Imgur.Endpoint.TopicListAPI
	fmt.Println(urlGet)
	req, err := http.NewRequest("GET", urlGet, nil)

	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	req.Header.Add("Authorization", configs.Imgur.ClientID)

	resp, err2 := client.Do(req)
	// handle error
	if err2 != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	// handle error
	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return
	}
	var responseTopicList configs.ResponseTopicListApi
	json.Unmarshal([]byte(body), &responseTopicList) //change raw to json
	renderJSONString, err := json.Marshal(responseTopicList)
	if err != nil {
		panic(err)
	}
	buffer := new(bytes.Buffer)
	crsfnya := c.XSRFToken()
	templateku.HalamanVar(renderJSONString, "ResponseTopicListApi", crsfnya, buffer)
	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	c.Ctx.Output.Header("text/html", "charset=utf-8")

}

func (c *TopicController) GetTopicGaleryItem() {
	type formIn struct {
		TopicID int
		ItemID  string
	}
	u := formIn{}
	if err := c.ParseForm(&u); err != nil {
		//handle error
		c.Ctx.WriteString("error parse form")
		return
	}

	client := &http.Client{}
	urlGet := configs.Imgur.Endpoint.TopicGaleryItemAPI + strconv.FormatInt(int64(u.TopicID), 10) + "/" + u.ItemID
	fmt.Println(urlGet)
	req, err := http.NewRequest("GET", urlGet, nil)
	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	req.Header.Add("Authorization", configs.Imgur.ClientID)
	resp, err2 := client.Do(req)
	// handle error

	if err2 != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	// handle error

	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return
	}
	var responseTopicGaleryItem configs.TopicGaleryItem
	json.Unmarshal([]byte(body), &responseTopicGaleryItem) //change raw to json
	renderJSONString, err := json.Marshal(responseTopicGaleryItem)
	if err != nil {
		panic(err)
	}
	// c.Ctx.WriteString(string(renderJSONString))
	// return

	buffer := new(bytes.Buffer)
	crsfnya := c.XSRFToken()
	templateku.HalamanVar(renderJSONString, "ResponseTopicGaleryItem", crsfnya, buffer)
	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")

}
func (c *TopicController) GetTopicDetail() {
	type formIn struct {
		PilihanTopik string
	}
	u := formIn{}
	if err := c.ParseForm(&u); err != nil {
		//handle error
		c.Ctx.WriteString("error parse form")
		return
	}
	if u.PilihanTopik == "" {
		c.Ctx.WriteString("form null")
		return
	}
	// 	c.Ctx.WriteString(u.PilihanTopik)
	// return
	client := &http.Client{}
	urlGet := configs.Imgur.Endpoint.TopicDetailAPI + u.PilihanTopik

	req, err := http.NewRequest("GET", urlGet, nil)
	fmt.Println(urlGet)
	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	req.Header.Add("Authorization", configs.Imgur.ClientID)

	resp, err2 := client.Do(req)
	// handle error
	if err2 != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	// handle error
	if err != nil {
		c.Ctx.WriteString("error:" + err.Error())
		return
	}
	var responseTopicDetail configs.ResponseTopicDetailApi
	json.Unmarshal([]byte(body), &responseTopicDetail) //change raw to json
	renderJSONString, err := json.Marshal(responseTopicDetail)
	if err != nil {
		panic(err)
	}
	buffer := new(bytes.Buffer)
	crsfnya := c.XSRFToken()
	// 	c.Ctx.WriteString(string(renderJSONString))
	// return
	templateku.HalamanVar(renderJSONString, "ResponseTopicDetailApi", crsfnya, buffer)
	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	c.Ctx.Output.Header("text/html", "charset=utf-8")

}
