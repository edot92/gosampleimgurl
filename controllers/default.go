package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	jwt "github.com/dgrijalva/jwt-go"
	configs "gitlab.com/edot92/gosampleimgurl/configs"
	models "gitlab.com/edot92/gosampleimgurl/models"
	templateku "gitlab.com/edot92/gosampleimgurl/views/go"
	"golang.org/x/crypto/bcrypt"
)

type MainController struct {
	beego.Controller
}
type UserLogin struct {
	Username string
	Password string
}

func (c *MainController) Get() {

	c.Ctx.Redirect(302, "topiclist")
}
func (c *MainController) GetLogin() {
	xsrf_token := c.XSRFToken()

	buffer := new(bytes.Buffer)
	templateku.Login(xsrf_token, buffer)

	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("text/html", "charset=utf-8")
}
func (c *MainController) PostLogin() {

	var formIn UserLogin
	err := c.ParseForm(&formIn)
	if err != nil {
		c.Ctx.WriteString("berhasil hehehehehhehe :)")
		return
	}

	isOk, passModel := models.GetUser(formIn.Username)
	if isOk == false {
		c.Ctx.WriteString("username tidak terdaftar")
		return
	}

	errPass := bcrypt.CompareHashAndPassword([]byte(passModel), []byte(formIn.Password))
	// if formIn.Username == "edy" && formIn.Password == "sundul" {
	if errPass == nil {
		token := jwt.New(jwt.SigningMethodHS256)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["nama"] = formIn.Username
		claims["role"] = "admin"
		claims["exp"] = time.Now().Add(time.Minute * 10).Unix()

		// Generate encoded token and send it as response.
		t, err := token.SignedString(configs.PrivateKey)
		if err != nil {
			c.Ctx.WriteString(err.Error())
			return

		}
		data, _ := json.Marshal(&t)
		url := "http://" + c.Ctx.Input.Domain() + ":" + strconv.Itoa(c.Ctx.Input.Port()) + "/topiclist?token=" + string(data)
		fmt.Println(url)
		// c.SetSession("token", data)

		c.Ctx.SetCookie("jwt", t)
		c.Ctx.Redirect(302, url)
		return

		// c.Ctx.WriteString(url)
	}
	c.Ctx.WriteString("username / password")
	return

}
