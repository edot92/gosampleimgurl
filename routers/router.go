package routers

import (
	"fmt"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/edot92/gosampleimgurl/configs"
	"gitlab.com/edot92/gosampleimgurl/controllers"
	uSerAgenCek "xojoc.pw/useragent"
)

func init() {
	var FilterUser = func(ctx *context.Context) {

		if strings.HasPrefix(ctx.Input.URL(), "/login") {
			return
		}

		userAgent := ctx.Input.UserAgent()
		if userAgent == "" {

		}
		ua := uSerAgenCek.Parse(userAgent)
		if ua == nil {
			ctx.Redirect(302, "/login?info=ua_nil")
			return

		}

		if ua.Security != uSerAgenCek.SecurityUnknown {
			ctx.Redirect(302, "/login?info=UASecurityUnknown")
			return

		}
		cokkieString := ctx.GetCookie("jwt")
		if cokkieString == "" {
			ctx.Redirect(302, "/login?info=cokkie is nul")
			return
		}

		token, err := jwt.Parse(cokkieString, func(token *jwt.Token) (interface{}, error) {

			return configs.PrivateKey, nil
		})

		if token.Valid {
			fmt.Println("valid")
			return
			// fmt.Println("tokennya masih valid")
		} else if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				// fmt.Println("Couldn't handle this token:", err)
				ctx.Redirect(302, "/login?info=timeout or invalid token")
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is ei`ther expired or not active yet
				// fmt.Println("Couldn't handle this token:", err)
				ctx.Redirect(302, "/login?info=timeout or invalid token")
				return
			} else {
				// fmt.Println("Couldn't handle this token:", err)
				ctx.Redirect(302, "/login?info="+err.Error())
			}
		} else {
			fmt.Println("Couldn't handle this token:", err)
			ctx.Redirect(302, "/login?info="+err.Error())
			return

		}

		if strings.HasPrefix(ctx.Input.URL(), "/register") {
			return
		}
		if strings.HasPrefix(ctx.Input.URL(), "/logout") {
			return
		}
		ctx.Redirect(302, "/login?info=uknow error")
		return
	}

	beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	beego.Router("/login", &controllers.MainController{}, "get:GetLogin;post:PostLogin")
	beego.Router("/", &controllers.TopicController{}, "get:GetTopicList")
	beego.Router("/topiclist", &controllers.TopicController{}, "get:GetTopicList")
	beego.Router("/topicdetail", &controllers.TopicController{}, "get:GetTopicDetail")
	beego.Router("/topicgaleryitem", &controllers.TopicController{}, "post:GetTopicGaleryItem")

}
